#include "htable.h"
#include "mylib.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define STR_MAX 80

struct htablerec {
    int capacity;
    int num_keys;
    int *frequencies;
    char **keys;
    HASHING_T method;
};

htable htable_new(int capacity, HASHING_T method) {
    int i;
    htable table = emalloc(sizeof *table);
    table->capacity = capacity;
    table->num_keys = 0;
    table->method = method;
    table->frequencies = emalloc(capacity * sizeof(int));
    for (i=0; i<capacity; i++) {
        table->frequencies[i] = 0;
    }
    table->keys = emalloc(capacity * sizeof(char));
    for (i=0; i<capacity; i++) {
        table->keys[i] = emalloc(STR_MAX * sizeof(char));
        strcpy(table->keys[i], "test");
    }
    return table;
}

void htable_print(htable table) {
    int i;
    printf("capacity is: %d\n", table->capacity);
    for (i=0; i<table->capacity; i++) {
        printf("%s\n", table->keys[i]);
    }
}
