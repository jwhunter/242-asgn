#ifndef HTABLE_H_
#define HTABLE_H_

typedef struct htablerec *htable;
typedef enum { LINEAR_P, DOUBLE } HASHING_T;

extern htable htable_new(int capacity, HASHING_T method);
extern void htable_print(htable table);

#endif
