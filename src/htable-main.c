#include "htable.h"
#include <stdlib.h>
#include <stdio.h>

int main(void) {
    HASHING_T method = LINEAR_P;
    htable table = htable_new(10, method);
    htable_print(table);
    return EXIT_SUCCESS;
}
